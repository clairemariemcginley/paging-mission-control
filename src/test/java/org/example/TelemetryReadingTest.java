package org.example;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class TelemetryReadingTest {

    private static HashMap<String, TelemetryReading> correctAnswers;

    // LOGGING (see notes) - testing tabbed formatting is not great
    private static final String EXPECTED_FORMAT = "\t{" +
            "\n\t\tsatelliteId: %s," +
            "\n\t\tseverity: \"%s\"," +
            "\n\t\tcomponent: \"%s\"," +
            "\n\t\ttimestamp: \"%s\"" +
            "\n\t}";

    @BeforeAll
    private static void setTestData() throws ParseException {
        correctAnswers = TestData.getCorrectAnswers();
    }

    @Test
    void calculateSeverity_SampleData() {
        for (Map.Entry<String, TelemetryReading> reading : correctAnswers.entrySet()) {
            TelemetryReading actualReading = new TelemetryReading(reading.getKey());
            TelemetryReading correctReading = reading.getValue();

            assertEquals(correctReading.getSeverity(), actualReading.getSeverity(),
                    actualReading + " - Parsing constructor calculates incorrect severity, OR getSeverity() returns incorrect calculation");
        }
    }

    @Test
    void getSatelliteId_SampleData() {
        for (Map.Entry<String, TelemetryReading> reading : correctAnswers.entrySet()) {
            TelemetryReading actualReading = new TelemetryReading(reading.getKey());
            TelemetryReading correctReading = reading.getValue();

            assertEquals(actualReading.getSatelliteId(), correctReading.getSatelliteId(),
                    "Parsing constructor calculates incorrect satelliteID, OR getSatelliteID() returns incorrect value");
        }
    }

    @Test
    void getComponent_SampleData() {
        for (Map.Entry<String, TelemetryReading> reading : correctAnswers.entrySet()) {
            TelemetryReading actualReading = new TelemetryReading(reading.getKey());
            TelemetryReading correctReading = reading.getValue();

            assertEquals(actualReading.getComponent(), correctReading.getComponent(),
                    actualReading + "Parsing constructor calculates incorrect component, OR getComponent() returns incorrect value");
        }
    }

    @Test
    void testToString_SampleData() {
        for (Map.Entry<String, TelemetryReading> reading : correctAnswers.entrySet()) {
            TelemetryReading actualReading = new TelemetryReading(reading.getKey());
            TelemetryReading correctReading = reading.getValue();

            String formattedCorrectAnswer = String.format(EXPECTED_FORMAT,
                    correctReading.getSatelliteId(),
                    correctReading.getSeverity(),
                    correctReading.getComponent(),
                    TestData.TIMESTAMP_FORMAT.format(correctReading.getTimestamp())
            );

            assertEquals(formattedCorrectAnswer, actualReading.toString(),
                    "Incorrect toString() formatting, OR incorrect timestamp, satelliteID, severity, component");
        }
    }

    @Test
    void confirmParsingConstructor_SampleData() {
        for (Map.Entry<String, TelemetryReading> reading : correctAnswers.entrySet()) {
            TelemetryReading correctReading = reading.getValue();
            TelemetryReading actualReading = new TelemetryReading(reading.getKey());

            assertEquals(actualReading, correctReading, "Parsing constructor returns incorrect values, OR equals() not overwritten");
        }
    }

    @Test
    void withinMinutes_True_SampleData() {
        final int MINUTE_TOLERANCE = 5;

        for (Map.Entry<String, TelemetryReading> reading : correctAnswers.entrySet()) {

            TelemetryReading actualReading = new TelemetryReading(reading.getKey());
            TelemetryReading correctReading = reading.getValue();

            assertNotNull(actualReading.getTimestamp(), "No timestamp provided in test value, OR incorrect getter");

            assertTrue(actualReading.withinMinutes(MINUTE_TOLERANCE, correctReading.getTimestamp()),
                    String.format("Test minutes: %d, Actual Reading TS: %d, Correct Reading TS: %d",
                        MINUTE_TOLERANCE,
                        actualReading.getTimestamp().getTime(),
                        correctReading.getTimestamp().getTime()));
        }
    }

    @Test
    void withinMinutes_False_ControlledData() throws ParseException {
        final int MINUTE_TOLERANCE = 5;

        TelemetryReading latestReading = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:20:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");
        TelemetryReading superOldReading = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20170101 01:01:01.000"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        long difference = Math.abs(superOldReading.getTimestamp().getTime() - latestReading.getTimestamp().getTime());

        assertFalse(latestReading.withinMinutes(MINUTE_TOLERANCE, superOldReading.getTimestamp()),
                String.format("Test minutes: %d, Difference: %d, Actual Reading TS: %d, Correct Reading TS: %d",
                    MINUTE_TOLERANCE,
                    difference,
                    latestReading.getTimestamp().getTime(),
                    superOldReading.getTimestamp().getTime()));

    }
}