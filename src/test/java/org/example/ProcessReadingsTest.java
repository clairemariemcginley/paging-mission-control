package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProcessReadingsTest {

    private static final int TOLERANCE_OCCURANCE = 3;
    private static final int MINUTE_FREQUENCY = 5;

    private static Queue<TelemetryReading> readings = new LinkedList<>();
    private static ProcessReadings processor;
    private static BufferedReader bufferedReaderSpy;

    @BeforeEach
    private void setTestData() throws IOException {
        readings = new LinkedList<>(TestData.getCorrectAnswers().values());

        BufferedReader bufferedReader = new BufferedReader(new StringReader(TestData.RAW_INPUT));
        bufferedReaderSpy = spy(bufferedReader);

        processor = new ProcessReadings(bufferedReaderSpy);
    }

    @Test
    void processStream_Runs_SampleData() throws Exception {
        processor.processStream();
    }

    @Test
    void processStream_CallForEachLine_SampleData() throws IOException {
        processor.processStream();
        verify(bufferedReaderSpy, times(15)).readLine();
    }

    @Test
    void processStream_RecordErrors_SampleData() throws IOException {
        List<TelemetryReading> errors = processor.processStream();
        assert(errors.size() == 2);
    }

    @Test
    void filterTimeWindow_True_SampleData() throws ParseException {
        String time = "20180101 23:07:09.521";
        processor.filterTimeWindow(readings, MINUTE_FREQUENCY, TestData.TIMESTAMP_FORMAT.parse(time));

        assertEquals(11, readings.size(), "3 readings should be filtered away from " + time);
    }

    @Test
    void filterTimeWindow_True_ControlledData() throws ParseException {
        Queue<TelemetryReading> controlledReadings = new LinkedList<>();
        TelemetryReading latestReading = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:20:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");
        TelemetryReading oldReading1 = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:10:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");
        TelemetryReading oldReading2 = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:05:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        controlledReadings.add(latestReading);

        processor.filterTimeWindow(controlledReadings, MINUTE_FREQUENCY, TestData.TIMESTAMP_FORMAT.parse("20180101 23:20:09.521"));

        assertEquals(controlledReadings.size(), 1, "Filtered queue does not match expected length");
        assertSame(controlledReadings.poll(), latestReading, "The latest reading is not correctly saved");
    }

    @Test
    void filterTimeWindow_False_ControlledData() throws ParseException {

        Queue<TelemetryReading> controlledReadings = new LinkedList<>();
        TelemetryReading latestReading = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:07:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");
        TelemetryReading latestReading1 = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:08:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");
        TelemetryReading latestReading2 = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:09:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        controlledReadings.add(latestReading);
        controlledReadings.add(latestReading1);
        controlledReadings.add(latestReading2);

        processor.filterTimeWindow(controlledReadings, MINUTE_FREQUENCY, TestData.TIMESTAMP_FORMAT.parse("20180101 23:10:09.521"));

        assertEquals(3, controlledReadings.size(), "Filtered queue does not match expected length");
        assertSame(controlledReadings.poll(), latestReading, "The latest reading is not correctly saved");
    }

    @Test
    void exceedTolerance_True_SampleData() throws ParseException {
        assert(processor.exceedTolerance(readings, TOLERANCE_OCCURANCE));
    }

    @Test
    void exceedTolerance_Matches_ControlledData() throws ParseException {
        Queue<TelemetryReading> controlledSampleReadings = new LinkedList<>();

        TelemetryReading error = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:01:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        controlledSampleReadings.add(error);
        controlledSampleReadings.add(error);
        controlledSampleReadings.add(error);

        assert(processor.exceedTolerance(controlledSampleReadings,3));}

    @Test
    void exceedTolerance_True_ControlledData() throws ParseException {
        Queue<TelemetryReading> controlledSampleReadings = new LinkedList<>();

        TelemetryReading error = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:01:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        controlledSampleReadings.add(error);
        controlledSampleReadings.add(error);

        assert(processor.exceedTolerance(controlledSampleReadings,1));
    }

    @Test
    void exceedTolerance_False_ControlledData() throws ParseException {
        Queue<TelemetryReading> controlledSampleReadings = new LinkedList<>();

        TelemetryReading batteryError = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:01:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        TelemetryReading thermostatError = new TelemetryReading(TestData.TIMESTAMP_FORMAT.parse("20180101 23:01:09.521"),
                1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW");

        controlledSampleReadings.add(batteryError);
        controlledSampleReadings.add(batteryError);

        controlledSampleReadings.add(thermostatError);
        controlledSampleReadings.add(thermostatError);

        assert(!processor.exceedTolerance(controlledSampleReadings,1000));
    }
}