package org.example;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {


    @Test
    void printFormatting() throws IOException {
        String expected = "[\n" +
                "  {\n" +
                "    \"satelliteId\" : 1000,\n" +
                "    \"severity\" : \"RED HIGH\",\n" +
                "    \"component\" : \"TSTAT\",\n" +
                "    \"timestamp\" : \"2018-01-01T23:03:05.009Z\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"satelliteId\" : 1000,\n" +
                "    \"severity\" : \"RED LOW\",\n" +
                "    \"component\" : \"BATT\",\n" +
                "    \"timestamp\" : \"2018-01-01T23:04:11.531Z\"\n" +
                "  }\n" +
                "]";


        List<TelemetryReading> telemetry = new ArrayList<>();
        telemetry.add(new TelemetryReading("20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT"));
        telemetry.add(new TelemetryReading("20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT"));

        String actual = Main.format(telemetry);

        assertEquals(expected, actual, "Output format does not match requirement");
    }

}