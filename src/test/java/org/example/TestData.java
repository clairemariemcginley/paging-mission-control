package org.example;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.TimeZone;

public class TestData {

    public static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

    /***********  FORMATTING **********
     public TelemetryReading(String satelliteId, Integer redHighLimit,
     Integer yellowHighLimit, Integer yellowLowLimit, Integer redLowLimit,
     Integer rawValue, String component, String severity)
     "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT"
     DATE|SATELLITE ID|RED HIGH|RED LOW|YELLOW HIGH|YELLOW LOW|READING|COMPONENT"
     ***********************************/

    public static final String RAW_INPUT =
            "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n" +
            "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT\n" +
            "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT\n" +
            "20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT\n" +
            "20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT\n" +
            "20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT\n" +
            "20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT\n" +
            "20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT\n" +
            "20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT\n" +
            "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT\n" +
            "20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT\n" +
            "20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT\n" +
            "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT\n" +
            "20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT\n";



    public static HashMap<String, TelemetryReading> getCorrectAnswers() {
        HashMap<String, TelemetryReading> correctAnswers = new HashMap<>();
        TIMESTAMP_FORMAT.setTimeZone(TimeZone.getTimeZone("Z"));

        try {
            correctAnswers.put("20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:01:05.001"),
                            1001, 101, 98, 25, 20, 99.9d, "TSTAT", "YELLOW HIGH")
            );
            correctAnswers.put("20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:01:09.521"),
                            1000, 17, 15, 9, 8, 7.8d, "BATT", "RED LOW")

            );
            correctAnswers.put("20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:01:26.011"),
                            1001, 101, 98, 25, 20, 99.8d, "TSTAT", "YELLOW HIGH")
            );
            correctAnswers.put("20180101 23:01:38.001|1000|101|98|25|20|102.9|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:01:38.001"),
                            1000, 101, 98, 25, 20, 102.9d, "TSTAT", "RED HIGH")
            );
            correctAnswers.put("20180101 23:01:49.021|1000|101|98|25|20|87.9|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:01:49.021"),
                            1000, 101, 98, 25, 20, 87.9d, "TSTAT", "NO ERROR")
            );
            correctAnswers.put("20180101 23:02:09.014|1001|101|98|25|20|89.3|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:02:09.014"),
                            1001, 101, 98, 25, 20, 89.3d, "TSTAT", "NO ERROR")
            );
            correctAnswers.put("20180101 23:02:10.021|1001|101|98|25|20|89.4|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:02:10.021"),
                            1001, 101, 98, 25, 20, 89.4d, "TSTAT", "NO ERROR")
            );
            correctAnswers.put("20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:02:11.302"),
                            1000, 17, 15, 9, 8, 7.7d, "BATT", "RED LOW")
            );
            correctAnswers.put("20180101 23:03:03.008|1000|101|98|25|20|102.7|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:03:03.008"),
                            1000, 101, 98, 25, 20, 102.7d, "TSTAT", "RED HIGH")
            );
            correctAnswers.put("20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:03:05.009"),
                            1000, 101, 98, 25, 20, 101.2, "TSTAT", "RED HIGH")
            );
            correctAnswers.put("20180101 23:04:06.017|1001|101|98|25|20|89.9|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:04:06.017"),
                            1001, 101, 98, 25, 20, 89.9d, "TSTAT", "NO ERROR")
            );
            correctAnswers.put("20180101 23:04:06.017|1000|17|15|9|8|7.9|BATT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:04:06.017"),
                            1000, 17, 15, 9, 8, 7.9d, "BATT", "RED LOW")
            );
            correctAnswers.put("20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:05:05.021"),
                            1001, 101, 98, 25, 20, 89.9d, "TSTAT", "NO ERROR")

            );
            correctAnswers.put("20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT",
                    new TelemetryReading(TIMESTAMP_FORMAT.parse("20180101 23:05:07.421"),
                            1001, 17, 15, 9, 8, 7.9d, "BATT", "RED LOW")
            );
        } catch (Exception e) {
            System.out.println("Issue static initialization of test data");
        }

        return correctAnswers;
    };
}
