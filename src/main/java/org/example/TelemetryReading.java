package org.example;

/****** FORMATTING ******
    <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
    20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

@JsonPropertyOrder({ "satelliteId", "severity", "component", "timestamp" })
public class TelemetryReading {

    public static final String SEVERITY_RED_HIGH = "RED HIGH";
    public static final String SEVERITY_YELLOW_HIGH = "YELLOW HIGH";
    public static final String SEVERITY_RED_LOW = "RED LOW";
    public static final String SEVERITY_YELLOW_LOW = "YELLOW LOW";
    public static final String NO_ERROR = "NO ERROR";

    public static final String BATTERY = "BATT";
    public static final String THERMOSTAT = "TSTAT";

    private static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date timestamp;

    private final Integer satelliteId;
    private final Integer redHighLimit;
    private final Integer yellowHighLimit;
    private final Integer yellowLowLimit;
    private final Integer redLowLimit;
    private final Double rawValue;
    private final String component;
    private final String severity;
    
    public TelemetryReading(String rawReading) {

        TIMESTAMP_FORMAT.setTimeZone(TimeZone.getTimeZone("Z"));
        String[] data = rawReading.split("\\|");


        // "20180101 23:05:05.021|1001|101|98|25|20|89.9|TSTAT"
        // DATE|SATELLITE ID|RED HIGH|RED LOW|YELLOW HIGH|YELLOW LOW|READING|COMPONENT"

        try {

            this.timestamp = TIMESTAMP_FORMAT.parse(data[0]);
        } catch (Exception e) {
            System.out.printf(
                    "Error parsing date, expected %s but was %s",
                    TIMESTAMP_FORMAT.toPattern(),
                    data[0]);
        }

        this.satelliteId = Integer.valueOf(data[1]);
        this.redHighLimit = Integer.valueOf(data[2]);
        this.yellowHighLimit = Integer.valueOf(data[3]);
        this.yellowLowLimit = Integer.valueOf(data[4]);
        this.redLowLimit = Integer.valueOf(data[5]);
        this.rawValue = Double.valueOf(data[6]);
        this.component = data[7];

        this.severity = calculateSeverity();
    }

    public TelemetryReading(Date timestamp, Integer satelliteId, Integer redHighLimit, Integer yellowHighLimit,
                            Integer yellowLowLimit, Integer redLowLimit, Double rawValue, String component, String severity) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
        this.severity = severity;
    }

    public String calculateSeverity() {
        String severity = "UNDEFINED";

        /*
            Severity Visualizer
            |   <- |RH|  <- |YH|   No Error   |YL| ->    |RL| ->
         */

        // Acceptable
        if(this.rawValue < this.yellowHighLimit && this.rawValue > this.yellowLowLimit) {
            severity = NO_ERROR;
        // Too High
        } else if(this.rawValue > this.redHighLimit) {
            severity = SEVERITY_RED_HIGH;
        // Warning High
        } else if(this.rawValue > yellowHighLimit) {
            severity = SEVERITY_YELLOW_HIGH;
        // Too Low
        } else if(this.rawValue < redLowLimit) {
            severity = SEVERITY_RED_LOW;
        // Warning Low
        } else if (this.rawValue < yellowLowLimit) {
            severity = SEVERITY_YELLOW_LOW;
        }

        return severity;
    }

    public boolean calculateCritical() {
        return this.severity.equals(SEVERITY_RED_HIGH) && this.component.equals(THERMOSTAT) ||
                this.severity.equals(SEVERITY_RED_LOW) && this.component.equals(BATTERY);
    }


    public boolean withinMinutes(int minutes, Date time) {
        final long MS_MINUTE_CONVERSION = 60000L;

        return Math.abs(this.timestamp.getTime() - time.getTime())
                < (minutes * MS_MINUTE_CONVERSION);
    }

    public Integer getSatelliteId() {
        return this.satelliteId;
    }

    public String getSeverity() {
        return this.severity;
    }

    public String getComponent() {
        return this.component;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }


    @Override
    public String toString() {
        // LOGGING (see notes) - Manually adding the new lines and tabs is not preferred
        return "\t{" +
                "\n\t\tsatelliteId: " + this.satelliteId + "," +
                "\n\t\tseverity: \"" + this.severity + "\"," +
                "\n\t\tcomponent: \"" + this.component + "\"," +
                "\n\t\ttimestamp: \"" + TIMESTAMP_FORMAT.format(this.timestamp) + "\"" +
                "\n\t}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TelemetryReading reading = (TelemetryReading) o;
        return timestamp.equals(reading.timestamp) && satelliteId.equals(reading.satelliteId) &&
                redHighLimit.equals(reading.redHighLimit) && yellowHighLimit.equals(reading.yellowHighLimit) &&
                yellowLowLimit.equals(reading.yellowLowLimit) && redLowLimit.equals(reading.redLowLimit) &&
                rawValue.equals(reading.rawValue) && component.equals(reading.component) && severity.equals(reading.severity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit,
                rawValue, component, severity);
    }
}
