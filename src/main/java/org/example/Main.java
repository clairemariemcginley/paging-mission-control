package org.example;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.util.*;

/*
    Satellite ground operations for an earth science mission
        - Monitors magnetic field variations at the Earth's poles
        - A pair of satellites fly in tandem orbit
        - At least one will have line of sight with a pole
    The satellite’s science instruments are sensitive to changes in temperature
        - Several temperature readings every minute
        - Battery systems voltage levels are also monitored

    Design a monitoring and alert application to processes status telemetry
    Generates alert messages in cases of certain limit violation scenarios

    Error States, for the same satellite
        - 3 readings below low limit within 5 minutes
        - 3 thermostat readings exceed high limit withing 5 minutes

    OUTPUT FORMAT
        [
            {
                "satelliteId": 1000,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": "2018-01-01T23:01:38.001Z"
            },
            ...
        ]
 */
public class Main {
    public static final String FILE_NAME = "telemetrydata.txt";

    public static void main(String[] args) throws Exception {

        InputStream in;
        File file = new File(FILE_NAME);
        in = new FileInputStream(file);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

        ProcessReadings processor = new ProcessReadings(bufferedReader);
        List<TelemetryReading> errors = processor.processStream();

        System.out.println(format(errors));

    }

    public static String format(List<TelemetryReading> errors) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        DefaultPrettyPrinter prettyPrinter = new DefaultPrettyPrinter();
        prettyPrinter.indentArraysWith(new DefaultPrettyPrinter.Lf2SpacesIndenter());

        return objectMapper.writer(prettyPrinter).writeValueAsString(errors);
    }
}