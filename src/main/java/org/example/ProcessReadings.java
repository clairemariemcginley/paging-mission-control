package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ProcessReadings {
    public static final int MINUTE_FREQUENCY = 5;
    public static final Long ERROR_TOLERANCE = 3L;

    private static final HashMap<Integer, Queue<TelemetryReading>> satelliteErrors = new HashMap<>();
    private final BufferedReader reader;

    public ProcessReadings(BufferedReader reader) {
        this.reader = reader;
    }

    public List<TelemetryReading> processStream() throws IOException {
        List<TelemetryReading> errors = new ArrayList<>();

        String line = reader.readLine();

        while(line != null) {
            TelemetryReading currentReading = new TelemetryReading(line);

            if(currentReading.calculateCritical()) {
                satelliteErrors.putIfAbsent(currentReading.getSatelliteId(), new LinkedList<>());
                Queue<TelemetryReading> individualSatellite = satelliteErrors.get(currentReading.getSatelliteId());
                individualSatellite.add(currentReading);

                filterTimeWindow(individualSatellite, MINUTE_FREQUENCY, currentReading.getTimestamp());

                if(exceedTolerance(individualSatellite, ERROR_TOLERANCE)) {
                    // LOGGING (See notes), while it's happening, matches format exactly
                    // System.out.println(currentReading);
                    errors.add(currentReading);
                }
            }

            line = reader.readLine();
        }

        return errors;
    }

    public void filterTimeWindow(Queue<TelemetryReading> satelliteQueue, Integer minuteWindow, Date time) {
        TelemetryReading pastReading = satelliteQueue.peek();
        while(pastReading != null && !pastReading.withinMinutes(minuteWindow, time)) {
            satelliteQueue.poll();
            pastReading = satelliteQueue.peek();
        }
    }

    public boolean exceedTolerance(Queue<TelemetryReading> satelliteQueue, long tolerance) {
        Map<String, Long> errorCounts = satelliteQueue.stream()
                .collect(Collectors.groupingBy(TelemetryReading::getComponent, Collectors.counting()));

        return errorCounts.getOrDefault(TelemetryReading.THERMOSTAT, 0L) >= tolerance
                || errorCounts.getOrDefault(TelemetryReading.BATTERY, 0L) >= tolerance;
    }
}
